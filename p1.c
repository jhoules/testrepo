#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

int copy(unsigned short new[], char orig[]);
void complement(char encrypt[], char input[]);
void leftShift(unsigned short ra[], int howmany, int shift);
void rightShift(unsigned short ra[], int howmany, int shift);
void print(unsigned short ra[], int howmany);
void mask(char [], int);

int main(void)
{
  const int maxchar = 81;
  char input[maxchar];
  char comp[maxchar];
  unsigned short shifted[maxchar];
  int shift=0, length=0;
  char value[maxchar];

  printf("Please enter a line of text for encryption.\n");
  fgets(input, maxchar, stdin);
  // the input includes the end of line character
  input[strlen(input)-1] = '\0';    // make null char overwrite end of line
  length = copy(shifted, input);    // will be used for 2nd encryption
  while (! (shift > 0 && shift < 8))
	{
	  printf("Enter a shift value between 1 and 7 inclusive: ");
	  shift = atoi(fgets(value, maxchar,stdin));     // converts to integer
	}

  printf("Your line of text:\n");
  printf("%s\n", input);
  complement(comp, input);
  printf("Complement encryption:\n");
  printf("%s\n", comp);
  complement(input, comp);
  printf("Complement decryption:\n");
  printf("%s\n", input);
  printf("Complement with 1st bit masked to be a 0:\n");
  mask(comp, pow(2,7)-1);
  printf("%s\n", comp);  
  printf("Masked complement decrypted:\n");
  complement(input, comp);
  printf("%s\n", input);

  printf("Your original text in integer form:\n");
  print(shifted, length);
  printf("Your input left shift encrypted by %d:\n", shift);
  leftShift(shifted, length, shift);
  print(shifted, length);
  printf("Your 2nd encryption decrypted:\n");
  rightShift(shifted, length, shift);
  print(shifted, length);
}

void mask(char ra[], int mask)
{
  for (int i=0; ra[i] != '\0'; i++)
	ra[i] = ra[i] & mask;
}

void leftShift(unsigned short ra[], int howmany, int shift)
{
  for (int i=0; i < howmany; i++)
	ra[i] = ra[i] << shift;
}

void rightShift(unsigned short ra[], int howmany, int shift)
{
  for (int i=0; i < howmany; i++)
	ra[i] = ra[i] >> shift;
}

void print(unsigned short ra[], int howmany)
{
  for (int i=0; i < howmany; i++)
	printf("%d ", ra[i]);
  printf("\n");
}

// most of this function came from Kernighan & Ritchie
int copy(unsigned short new[], char orig[])
{
  int i = 0;
  while((new[i] = orig[i]) != '\0')
	++i;
  return i;
}

void complement(char encrypt[], char input[])
{
  int i = 0;
  while(input[i++] != '\0')
    {
      if (isspace(input[i]))
          encrypt[i] = input[i];
      else
          encrypt[i] = ~(input[i]);
    }
  encrypt[i] = '\0';
}
