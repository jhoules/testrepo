/*
  This is an incomplete program to compute the GPA of a set of courses.
  gpa.in contains sample input
  gpa.out contains sample output
  In order to use them with this program, you must use file I/O redirection in Unix
*/

#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define GSIZE 3

// function declarations = prototypes
double getpoints(char []);  // no param names needed

int main(void)
{
    char ch, grade[GSIZE];
    int i;
    int credits;
    double points;
    double totalpoints = 0, totalcredits = 0;

    puts("enter grades and credits: ");

    // repeat until end of input
    while ((ch  = getchar()) != EOF) {
        // get grade
        i = 0;
        while (!isspace(ch)) {
            grade[i++] = ch;
            ch = getchar();
        }
        grade[i] = '\0';
        points = getpoints(grade);

        // skip whitespace
        while (isspace(ch)) {
            ch = getchar(); 
        } 

        // read credits
        credits = 0;
        while (isdigit(ch)) {
            credits = credits * 10 + ch - '0';
            ch = getchar();
        }

        // go to next line
        while (ch != '\n')
            ch = getchar();

        printf("grade is: %-2s credits = %4d points = %4.1f \n", grade, credits, points);
    } // while not EOF

	return 0;  // 0 means no errors
} // main

double getpoints(char g[])
{
    double points = 0;
    g[0] = toupper(g[0]);
    switch (g[0]) {
    case 'A': points = 4; break;
    case 'B': points = 3; break;
    case 'C': points = 2; break;
    case 'D': points = 1; break;
    default: points = 0;
    }

    if (g[1] == '+' && 'B' <= g[0] && g[0] <= 'D')
        points += .3;
    if (g[1] == '-' && 'A' <= g[0] && g[0] <= 'C')
        points -= .3;
    return points;
}
